{*******************************************************}
{                                                       }
{       SreachForm.pas
{       Copyright @2014/6/9 14:14:20 by 姜梁
{                                                       }
{       功能描述：
{	函数说明：
{*******************************************************}

unit SreachForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ComCtrls, Generics.Collections, DiskData,
  SreachThread;

type
  TfrmSreach = class(TForm)
    btnStart: TButton;
    btnCancel: TButton;
    pnlBottom: TPanel;
    pnlMain: TPanel;
    lstDisk: TListBox;
    pnlLeft: TPanel;
    lblDisk: TLabel;
    lblTitle: TLabel;
    pbProgress: TProgressBar;
    lblSiteTile: TLabel;
    lblDiskSite: TLabel;
    procedure FormShow(Sender: TObject);
    procedure btnStartClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure lstDiskClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnCancelClick(Sender: TObject);
  private
    CS: TRTLCriticalSection;

    DiskStrList: TStrings;
    IsFinsh: array of Boolean;
    FileInfoList: TList < TFileList > ;
    ThreadList: TList < TSreachThread > ;
    procedure GetMessage(nowInt, myThreadID: Integer; siteStr: string);
  public
    procedure InitForm(disklist: TStrings; filelist: TList < TFileList > );
  end;

var
  frmSreach: TfrmSreach;

implementation

{$R *.dfm}

{ TfrmSreach }

procedure TfrmSreach.btnCancelClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmSreach.btnStartClick(Sender: TObject);
var
  I: Integer;
begin
  if btnStart.Caption = '开始' then
  begin
    btnStart.Enabled := False;
    btnStart.Caption := '完成';

    for I := 0 to ThreadList.Count - 1 do
    begin
      ThreadList[I].Resume;
    end;
  end
  else
  begin
    Close;
  end;
end;

{$REGION '窗体事件'}

procedure TfrmSreach.FormClose(Sender: TObject; var Action: TCloseAction);
var
  I: Integer;
begin
  for I := 0 to ThreadList.Count - 1 do
  begin
    ThreadList[I].Terminate;
  end;
end;

procedure TfrmSreach.FormDestroy(Sender: TObject);
var
  I: Integer;
begin
  DeleteCriticalSection(CS);

  for I := 0 to ThreadList.Count - 1 do
  begin
    ThreadList[I].Free;
  end;

  ThreadList.Free;
end;

procedure TfrmSreach.FormShow(Sender: TObject);
var
  I: Integer;
  tempThread: TSreachThread;
begin
  ThreadList := TList < TSreachThread > .Create;

  for I := 0 to DiskStrList.Count - 1 do
  begin
    lstDisk.Items.Add(DiskStrList[I]);

    tempThread := TSreachThread.Create(DiskStrList[I], FileInfoList[I], I,
      Handle);
    tempThread.MySreachEvent := GetMessage;
    ThreadList.Add(tempThread);
  end;

  pbProgress.Max := 100000;
  lstDisk.Selected[0] := True;
end;
{$ENDREGION}

{$REGION '私有函数'}

procedure TfrmSreach.GetMessage(nowInt, myThreadID: Integer; siteStr: string);
var
  I: Integer;
begin
  if nowInt = -1 then
  begin
    IsFinsh[myThreadID] := True;
    if myThreadID = lstDisk.ItemIndex then
    begin
      lstDiskClick(nil);
    end;

    for I := 0 to High(IsFinsh) - 1 do
    begin
      if IsFinsh[I] = False then
      begin
        Break;
      end;

      if I = High(IsFinsh) - 1 then
      begin
        btnStart.Enabled := True;
      end;

    end;
  end
  else
  begin
    if myThreadID <> lstDisk.ItemIndex then Exit;

    EnterCriticalSection(CS);

    pbProgress.Position := nowInt;
    lblDiskSite.Caption := siteStr;

    LeaveCriticalSection(CS);
  end
end;

procedure TfrmSreach.InitForm(disklist: TStrings; filelist: TList < TFileList >
  );
begin
  DiskStrList := disklist;
  FileInfoList := filelist;

  SetLength(IsFinsh, DiskStrList.Count);
  InitializeCriticalSection(CS);
end;

procedure TfrmSreach.lstDiskClick(Sender: TObject);
begin
  if lstDisk.ItemIndex <> -1 then
  begin
    if IsFinsh[lstDisk.ItemIndex] then
    begin
      pbProgress.Position := pbProgress.Max;
      lblDiskSite.Caption := '该磁盘扫描完毕，共计' +
        IntToStr(FileInfoList[lstDisk.ItemIndex].Count) + '个文件';
    end;
  end;
end;

{$ENDREGION}
end.

