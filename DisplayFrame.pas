unit DisplayFrame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, DiskData, SreachTool;

type
  TMyDisplayFrame = class(TFrame)
    lvDislpay: TListView;
    procedure lvDislpayData(Sender: TObject; Item: TListItem);
  private
    myFileList:TFileList;
  public
    procedure SetData(FileList: TFileList);
    procedure CleanData();
  end;

implementation

{$R *.dfm}

{ TMyDisplayFrame }

procedure TMyDisplayFrame.CleanData;
begin
  lvDislpay.Items.Clear;
end;

procedure TMyDisplayFrame.lvDislpayData(Sender: TObject; Item: TListItem);
begin
  if not Assigned(myFileList) then Exit;
  if myFileList.Count < Item.Index then Exit;

  Item.Caption := myFileList[Item.Index].FileName;
  Item.SubItems.Add(GetSize(myFileList[Item.Index].FileSize));
  Item.SubItems.Add(myFileList[Item.Index].FileSite);
  Item.SubItems.Add(IntToStr(myFileList[Item.Index].FileSize));
end;

procedure TMyDisplayFrame.SetData(FileList: TFileList);
var
  I: Integer;
begin
  myFileList:= FileList;
  lvDislpay.Items.Count := FileList.Count;
end;

end.

