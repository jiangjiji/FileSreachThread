{*******************************************************}
{                                                       }
{       DiskData.pas
{       Copyright @2014/6/9 10:55:01 by 姜梁
{                                                       }
{       功能描述：文件扫描信息保存类
{	函数说明：
{*******************************************************}

unit DiskData;

interface

uses
  Classes, Windows,Generics.Collections;

type
  FileMessage = record
    FileName:string;
    FileSite:string;
    FileSize:Int64;
  end;

type
  TFileList = TList<FileMessage>;

implementation

end.
