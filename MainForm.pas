{*******************************************************}
{                                                       }
{       MainForm.pas
{       Copyright @2014/6/9 10:55:32 by 姜梁
{                                                       }
{       功能描述：文件扫描
{	函数说明：
{*******************************************************}

unit MainForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, DiskData, Generics.Collections ,SreachTool, SreachForm,
  ComCtrls,DisplayFrame;

type
  TfrmMain = class(TForm)
    mmMenu: TMainMenu;
    mniStartBtn: TMenuItem;
    pgcDisk: TPageControl;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure mniStartBtnClick(Sender: TObject);
  private
    DiskStrList: TStrings;
    FileInfoList: TList<TFileList>;

    procedure SetData();
    procedure CleanData();
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

implementation

{$R *.dfm}

{$REGION '窗体事件'}

procedure TfrmMain.FormCreate(Sender: TObject);
var
  I: Integer;
  tempList:TFileList;
  tabpage:TTabsheet;
  MyDisplayFrame:TMyDisplayFrame;
begin
  DiskStrList:= GetAllDiskName;
  FileInfoList:= TList<TFileList>.Create;

  for I := 0 to DiskStrList.Count - 1 do
  begin
    tempList:= TFileList.Create;
    FileInfoList.Add(tempList);

    tabpage:= TTabSheet.Create(Self);
    tabpage.Caption:= DiskStrList[I]+'盘';
    tabpage.PageControl := pgcDisk;

    MyDisplayFrame:= TMyDisplayFrame.Create(Self) ;
    MyDisplayFrame.Name:= 'DisplayFrame'+inttostr(I);
    MyDisplayFrame.Align := alClient;
    MyDisplayFrame.Parent := tabpage;
  end;
end;

procedure TfrmMain.FormDestroy(Sender: TObject);
var
  I: Integer;
begin
  DiskStrList.Free;

  for I := 0 to FileInfoList.Count - 1 do
  begin
    FileInfoList[I].Free;
  end;
  FileInfoList.Free;
end;
{$ENDREGION}

{$REGION '窗体btn'}
procedure TfrmMain.mniStartBtnClick(Sender: TObject);
var
  MySreachForm:TfrmSreach;
  I: Integer;
begin
  CleanData;

  MySreachForm:= TfrmSreach.Create(Self);

  MySreachForm.InitForm(DiskStrList,FileInfoList);
  MySreachForm.ShowModal;

  MySreachForm.Free;
  SetData();
end;

procedure TfrmMain.SetData;
var
  I: Integer;
  myTemp:TMyDisplayFrame;
begin
  for I := 0 to pgcDisk.PageCount - 1 do
  begin
    myTemp:= TMyDisplayFrame(pgcDisk.Pages[I].Controls[0]);
    myTemp.SetData(FileInfoList[I]);;
  end;
end;

procedure TfrmMain.CleanData;
var
  I: Integer;
  myTemp:TMyDisplayFrame;
begin
  for I := 0 to pgcDisk.PageCount - 1 do
  begin
    myTemp:= TMyDisplayFrame(pgcDisk.Pages[I].Controls[0]);
    myTemp.CleanData;
  end;

  for I := 0 to FileInfoList.Count - 1 do
  begin
    FileInfoList[I].Clear;
  end;
end;

{$ENDREGION}

end.
