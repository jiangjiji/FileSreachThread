unit SreachTool;

interface

uses
  Classes, Windows,SysUtils;

function GetAllDiskName():TStrings;
function GetSize(size:Int64):string;

implementation

/// <summary>
///  取得所有系统盘符
/// </summary>
/// <returns>返回盘符字符串</returns>
function GetAllDiskName():TStrings;
var
  I: integer;
  tempString: string;
  DiskList:TStrings;
begin
  DiskList:= TStringList.Create;

  for I := 65 to 90 do
  begin
    //GetDriveType获取盘符信息
    //0 驱动器类型不确定
    //1 系统目录不存在
    //2 DRIVE_REMOVABLE 是可移动驱动器
    //3 DRIVE_FIXED 是固定驱动器
    //4 DRIVE_REMOTE 是网络驱动器
    //5 DRIVE_CDROM 是CD-ROM驱动器
    //6 DRIVE_RAMDISK 是虚拟驱动器
    tempString:= chr(I) + ':';
    if  GetDriveType(Pchar(tempString)) = 3 then DiskList.Add(tempString);
  end;

  Result:= DiskList;
end;

function GetSize(size:Int64):string;
var
  num:Double;
begin
  num:= size;
  if num > 1024 then
  begin
    num:= num/1024;
    if num > 1024 then
    begin
      num:= num/1024;
      if num > 1024 then
      begin
        num:= num/1024;
        Result:= FloatToStrF(num,ffNumber,6,2) + 'GB';
      end else
      begin
        Result:= FloatToStrF(num,ffNumber,6,2) + 'MB';
      end;
    end else
    begin
      Result:= FloatToStrF(num,ffNumber,6,2) + 'KB';
    end;
  end else
  begin
    Result:= FloatToStrF(num,ffNumber,4,0) + 'Bytes';
  end;

end;
end.
