unit SreachThread;

interface

uses
  Classes, SysUtils, DiskData, Windows, Messages, Generics.Defaults, ComCtrls,
  StdCtrls;

type
  TSreachEvent = procedure(nowInt, myThreadID: Integer; siteStr: string)
    of object;

type
  TSreachThread = class(TThread)
  private
    myThreadID: Integer;
    mainHandle: Cardinal;
    nowIndex: Integer;
    DiskStr: string;
    MyFileList: TFileList;
    procedure FileSreachFunc(diskPath: string);
  protected
    procedure Execute; override;
  public
    MySreachEvent: TSreachEvent;
    constructor Create(thisdiskstr: string; filelist: TFileList; thisthreadID:
      Integer; thishanlde: Cardinal); overload;
  end;

type
  TFileDataComparer = class(TComparer < FileMessage > )
  public
    function Compare(const Left, Right: FileMessage): Integer; override;
  end;

implementation

{ TSreachThread }

constructor TSreachThread.Create(thisdiskstr: string; filelist: TFileList;
  thisthreadID: Integer; thishanlde: Cardinal);
begin
  DiskStr := thisdiskstr;
  MyFileList := filelist;
  myThreadID := thisthreadID;
  mainHandle := thishanlde;
  nowIndex := 0;

  inherited Create(True);
end;

procedure TSreachThread.Execute;
var
  mySort:TFileDataComparer;
begin
  FileSreachFunc(DiskStr);

  mySort:= TFileDataComparer.Create;
  MyFileList.Sort(mySort);
  mySort.Free;
  MySreachEvent(-1, myThreadID, '');
end;

procedure TSreachThread.FileSreachFunc(diskPath: string);
var
  MySearchInfo: TSearchRec;
  tempFileInfo: FileMessage;
  tempPath: string;
  found: Integer;
begin
  if Terminated then Exit;

  //加上搜索后缀,得到类似‘c:\*.*‘ 、‘c:\windows\*.*‘的搜索路径
  tempPath := diskPath + '\*.*';
  try
    //在当前目录查找第一个文件、子目录
    found := FindFirst(tempPath, faAnyFile, MySearchInfo);

    //找到了一个文件或目录后
    while found = 0 do
    begin
      if Terminated then Exit;
      //如果找到的是个目录
      if (MySearchInfo.Attr and faDirectory) <> 0 then
      begin
        {在搜索非根目录(C:\、D:\)下的子目录时会出现‘.‘,‘..‘的"虚拟目录"
        大概是表示上层目录和下层目录吧。。。要过滤掉才可以}
        if (MySearchInfo.Name <> '.') and (MySearchInfo.Name <> '..') then
        begin
          {由于查找到的子目录只有个目录名，所以要添上上层目录的路径
          searchRec.Name = ‘Windows‘;tmpStr:=‘c:\Windows‘;
          加个断点就一清二楚了}
          tempPath := diskPath + '\' + MySearchInfo.Name;

          if (nowIndex mod 101) = 1  then
          MySreachEvent(nowIndex, myThreadID, diskPath);
          Inc(nowIndex);
          //自身调用，查找子目录，递归
          FileSreachFunc(tempPath);
        end;
      end
      else
        //如果找到的是个文件 递归的结束条件
      begin
        //过滤系统文件和隐藏文件 faSysFile + faHidden = 6
        if MySearchInfo.Attr and 6 = 0 then
        begin
          //把找到的文件加到list中
          tempFileInfo.FileName := MySearchInfo.Name;
          tempFileInfo.FileSize := MySearchInfo.Size;
          tempFileInfo.FileSite := diskPath;
          MyFileList.Add(tempFileInfo);
        end;
      end;
      //查找下一个文件或目录
      found := FindNext(MySearchInfo);
    end;
  finally
    SysUtils.FindClose(MySearchInfo);
  end;
end;

{ TFileDataComparer }

function TFileDataComparer.Compare(const Left, Right: FileMessage): Integer;
begin
  if Left.FileSize > Right.FileSize then
    Result := -1
  else if Left.FileSize < Right.FileSize then
    Result := 1
  else
    Result := 0;
end;

end.

