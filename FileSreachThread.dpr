program FileSreachThread;

uses
  FastMM4,
  Forms,
  MainForm in 'MainForm.pas' {frmMain},
  SreachForm in 'SreachForm.pas' {frmSreach},
  DiskData in 'DiskData.pas',
  SreachTool in 'SreachTool.pas',
  SreachThread in 'SreachThread.pas',
  DisplayFrame in 'DisplayFrame.pas' {MyDisplayFrame: TFrame};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
