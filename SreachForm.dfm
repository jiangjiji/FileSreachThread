object frmSreach: TfrmSreach
  Left = 0
  Top = 0
  Caption = #24320#22987#26597#25214
  ClientHeight = 362
  ClientWidth = 584
  Color = clBtnFace
  DoubleBuffered = True
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnlBottom: TPanel
    Left = 0
    Top = 302
    Width = 584
    Height = 60
    Align = alBottom
    TabOrder = 0
    DesignSize = (
      584
      60)
    object btnStart: TButton
      Left = 382
      Top = 17
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = #24320#22987
      TabOrder = 0
      OnClick = btnStartClick
    end
    object btnCancel: TButton
      Left = 486
      Top = 17
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = #21462#28040
      TabOrder = 1
      OnClick = btnCancelClick
    end
  end
  object pnlMain: TPanel
    Left = 0
    Top = 0
    Width = 584
    Height = 302
    Align = alClient
    TabOrder = 1
    object lblTitle: TLabel
      Left = 200
      Top = 16
      Width = 60
      Height = 13
      Caption = #25195#25551#36827#24230#65306
    end
    object lblSiteTile: TLabel
      Left = 200
      Top = 88
      Width = 60
      Height = 13
      Caption = #27491#22312#25195#25551#65306
    end
    object lblDiskSite: TLabel
      Left = 203
      Top = 120
      Width = 358
      Height = 162
      AutoSize = False
      WordWrap = True
    end
    object pnlLeft: TPanel
      Left = 1
      Top = 1
      Width = 185
      Height = 300
      Align = alLeft
      Caption = 'pnlLeft'
      TabOrder = 0
      object lblDisk: TLabel
        Left = 11
        Top = 14
        Width = 72
        Height = 13
        Caption = #25195#25551#30340#30913#30424#65306
      end
      object lstDisk: TListBox
        Left = 11
        Top = 42
        Width = 158
        Height = 239
        Align = alCustom
        ItemHeight = 13
        TabOrder = 0
        OnClick = lstDiskClick
      end
    end
    object pbProgress: TProgressBar
      Left = 200
      Top = 43
      Width = 361
      Height = 17
      TabOrder = 1
    end
  end
end
